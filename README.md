# GpxVid

GpxVid is a graphical software to see a position on an interactive map while watching a video.

GpxVid is a combination between a video player and an interactive map displaying a GPX track.

If you recorded a GPX track and have a corresponding video footage, you may want to watch them in the same interface.

When a video is played, a marker is moving on the map.

![gpxvid1](https://gitlab.com/eneiluj/gpxvid/uploads/ac65eafcc6667777968170e6219366db/gpxvid1.png)

This program uses PyQt5, Leaflet, Leaflet.Gpx, GpxPy.

# Usage

Load a GPX file and then a video file, that's pretty much it.

When the video is playing, GPX track and video are moving together. If the track and the video are not synchronized,
you need to pause the video, then the track and the video sliders can move independently.

All control shortcuts are listed in the top menus.

# Dependencies

GpxVid works with Python2 or Python3. It needs PyQt5 with QtMultimedia and QtWebkit support.

## GNU/Linux

### Debian/Ubuntu
```
sudo apt install python3-pyqt5 python3-pyqt5.qtmultimedia python3-pyqt5.qtwebkit
```

### Fedora/CentOS

```
sudo yum install python3-qt5 PyQt5-webkit
```
or if it is not available :
```
sudo yum install python-qt5 PyQt5-webkit
```

## MacOS

Use MacPorts or Fink or HomeBrew to install PyQt5.

## Windows

It should work. If someone knows how to install Python3 + PyQt5 and make it work under Windows, tell me please.

