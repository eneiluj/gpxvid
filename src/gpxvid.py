#! /usr/bin/env python
#-*- coding: utf-8 -*-

import sys, os, time, math
import gpxpy, gpxpy.gpx
#import inspect
from PyQt5.QtWidgets import QApplication
from PyQt5 import QtCore, QtGui
from PyQt5.QtCore import  *
from PyQt5.QtGui import *
from PyQt5 import uic
from PyQt5 import QtCore, QtGui, QtWebKit, QtNetwork, QtWidgets, QtWebKitWidgets
from PyQt5.QtCore import QFile, QCoreApplication
from PyQt5.QtWidgets import QApplication, QVBoxLayout, QHBoxLayout, QLabel, QGraphicsScene
from PyQt5.QtWidgets import QSizePolicy, QFileDialog, QStyle, QStyleFactory, QMessageBox, QSpacerItem
from PyQt5.QtMultimedia import *
from PyQt5.QtMultimediaWidgets import *
import functools

versionFilePath = '%s/version.txt'%os.path.dirname(os.path.realpath(sys.argv[0]))
if os.path.exists(versionFilePath):
    f = open(versionFilePath, 'r')
    VERSION = f.readline().strip()
    VERSION_DATE = f.readline().strip()
    f.close()

class MyQVideoWidget(QVideoWidget):
    def __init__(self, parent=None):
        super(MyQVideoWidget, self).__init__(parent)
        self.gpxvid = parent

    def Video_Widget(self, parent):
        self.Video_Player = QVideoWidget(parent)
        self.Video_Player.setObjectName("videoPlayer")

    def keyPressEvent(self, event):
        if event.key() == Qt.Key_Escape and self.isFullScreen():
            self.setFullScreen(False)
            event.accept()
        elif event.key() == Qt.Key_Space:
            self.gpxvid.playPauseControl()
        elif event.key() == Qt.Key_F and event.modifiers() & Qt.CTRL:
            self.setFullScreen(False)
            event.accept()
        elif event.key() == Qt.Key_Enter and event.modifiers() & Qt.Key_Alt:
            self.setFullScreen(not self.isFullScreen())
            event.accept()
        elif event.key() == Qt.Key_Right and event.modifiers() & Qt.SHIFT:
            self.gpxvid.plus5sec()
        elif event.key() == Qt.Key_Left and event.modifiers() & Qt.SHIFT:
            self.gpxvid.minus5sec()
        elif event.key() == Qt.Key_Right and event.modifiers() & Qt.ALT:
            self.gpxvid.plus10sec()
        elif event.key() == Qt.Key_Left and event.modifiers() & Qt.ALT:
            self.gpxvid.minus10sec()
        elif event.key() == Qt.Key_Right and event.modifiers() & Qt.CTRL:
            self.gpxvid.plus1min()
        elif event.key() == Qt.Key_Left and event.modifiers() & Qt.CTRL:
            self.gpxvid.minus1min()

    def mouseDoubleClickEvent(self, event):
        self.setFullScreen(not self.isFullScreen())
        event.accept()

def distance(p1, p2):
    """ return distance between these two gpx points in meters
    """

    lat1 = p1.latitude
    long1 = p1.longitude
    lat2 = p2.latitude
    long2 = p2.longitude

    if (lat1 == lat2 and long1 == long2):
        return 0

    # Convert latitude and longitude to
    # spherical coordinates in radians.
    degrees_to_radians = math.pi/180.0

    # phi = 90 - latitude
    phi1 = (90.0 - lat1)*degrees_to_radians
    phi2 = (90.0 - lat2)*degrees_to_radians

    # theta = longitude
    theta1 = long1*degrees_to_radians
    theta2 = long2*degrees_to_radians

    # Compute spherical distance from spherical coordinates.

    # For two locations in spherical coordinates
    # (1, theta, phi) and (1, theta, phi)
    # cosine( arc length ) =
    #    sin phi sin phi' cos(theta-theta') + cos phi cos phi'
    # distance = rho * arc length

    cos = (math.sin(phi1)*math.sin(phi2)*math.cos(theta1 - theta2) +
           math.cos(phi1)*math.cos(phi2))
    # why some cosinus are > than 1 ?
    if cos>1.0:
        cos=1.0
    arc = math.acos( cos )

    # Remember to multiply arc by the radius of the earth
    # in your favorite set of units to get length.
    return arc*6371000

formGpxvid,  baseGpxvid = uic.loadUiType('%s/uis/gpxvid.ui'%os.path.dirname(os.path.realpath(sys.argv[0])))

class Gpxvid(formGpxvid, baseGpxvid):
    def __init__(self, app, gpxpath='', videopath='', parent=None):
        super(Gpxvid, self).__init__(parent)
        self.app = app
        self.videopath = videopath
        self.gpxpath = gpxpath

        self.providerToUrl = {
                'OpenStreetMap' : 'http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',
                'OpenCycleMap' : 'http://{s}.tile.opencyclemap.org/cycle/{z}/{x}/{y}.png',
                'OpenTopoMap' : 'http://{s}.tile.opentopomap.org/{z}/{x}/{y}.png',
                'ESRI WorldImagery' : 'http://server.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/{z}/{y}/{x}'
        }
        self.providerToAttrib = {
                'OpenStreetMap' : '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>',
                'OpenCycleMap' : '&copy; <a href="http://www.opencyclemap.org">OpenCycleMap</a>, &copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>',
                'OpenTopoMap' : 'Map data: &copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>, <a href="http://viewfinderpanoramas.org">SRTM</a> | Map style: &copy; <a href="https://opentopomap.org">OpenTopoMap</a> (<a href="https://creativecommons.org/licenses/by-sa/3.0/">CC-BY-SA</a>)',
                'ESRI WorldImagery' : 'Tiles &copy; Esri &mdash; Source: Esri, i-cubed, USDA, USGS, AEX, GeoEye, Getmapping, Aerogrid, IGN, IGP, UPR-EGP, and the GIS User Community'
        }

        self.gpxCurrentSecond = 0
        self.lastVideoPosition = 0
        self.lastGpxPosition = 0
        self.secondToCoords = {}

        self.createWidgets()
        self.setWindowTitle("GpxVid")

    def createWidgets(self):
        self.ui = self
        self.ui.setupUi(self)

        self.ui.spacer = QSpacerItem(1, 1, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.ui.toolBar.hide()
        self.ui.statusBar.hide()

        file_menu = self.ui.menubar.addMenu("&File")
        self.file_menu = file_menu
        action = file_menu.addAction(self.style().standardIcon(QStyle.SP_DirOpenIcon),"Load &Gpx file",self.loadGpx,QKeySequence(Qt.CTRL + Qt.Key_G))
        action = file_menu.addAction(self.style().standardIcon(QStyle.SP_DirOpenIcon),"Load V&ideo file",self.loadVideo,QKeySequence(Qt.CTRL + Qt.Key_I))
        action = file_menu.addAction(self.style().standardIcon(QStyle.SP_TitleBarCloseButton),"&Quit",self.close,QKeySequence(Qt.CTRL + Qt.Key_Q))

        play_menu = self.ui.menubar.addMenu("&Video")
        play_menu.addAction(self.style().standardIcon(QStyle.SP_TitleBarMaxButton),"Fullscreen",self.videoFullScreen,QKeySequence(Qt.CTRL + Qt.Key_F))
        play_menu.addAction(self.style().standardIcon(QStyle.SP_MediaSeekBackward),"-5 sec",self.minus5sec,QKeySequence(Qt.SHIFT + Qt.Key_Left))
        play_menu.addAction(self.style().standardIcon(QStyle.SP_MediaSeekForward),"+5 sec",self.plus5sec,QKeySequence(Qt.SHIFT + Qt.Key_Right))
        play_menu.addAction(self.style().standardIcon(QStyle.SP_MediaSeekBackward),"-10 sec",self.minus10sec,QKeySequence(Qt.ALT + Qt.Key_Left))
        play_menu.addAction(self.style().standardIcon(QStyle.SP_MediaSeekForward),"+10 sec",self.plus10sec,QKeySequence(Qt.ALT + Qt.Key_Right))
        play_menu.addAction(self.style().standardIcon(QStyle.SP_MediaSeekBackward),"-1 min",self.minus1min,QKeySequence(Qt.CTRL + Qt.Key_Left))
        play_menu.addAction(self.style().standardIcon(QStyle.SP_MediaSeekForward),"+1 min",self.plus1min,QKeySequence(Qt.CTRL + Qt.Key_Right))
        play_menu.addAction(self.style().standardIcon(QStyle.SP_MediaVolume),"-5% volume",self.minus5volume,QKeySequence(Qt.ALT + Qt.Key_Down))
        play_menu.addAction(self.style().standardIcon(QStyle.SP_MediaVolume),"+5% volume",self.plus5volume,QKeySequence(Qt.ALT + Qt.Key_Up))
        play_menu.addAction(self.style().standardIcon(QStyle.SP_MediaVolumeMuted),"Mute",self.mutePressed,QKeySequence(Qt.CTRL + Qt.Key_M))

        map_menu = self.ui.menubar.addMenu("&Map")
        map_menu.addAction(self.style().standardIcon(QStyle.SP_TitleBarMinButton),"Zoom out",self.zoomOut,QKeySequence(Qt.CTRL + Qt.Key_Minus))
        map_menu.addAction(self.style().standardIcon(QStyle.SP_TitleBarMaxButton),"Zoom in",self.zoomIn,QKeySequence(Qt.CTRL + Qt.Key_Plus))
        map_menu.addAction(self.style().standardIcon(QStyle.SP_FileLinkIcon),"Toggle follow marker",self.toggleFollow,QKeySequence(Qt.CTRL + Qt.Key_T))

        interf_menu = self.ui.menubar.addMenu("&Interface")
        interf_menu.addAction(self.style().standardIcon(QStyle.SP_ArrowLeft),"Move splitter left",self.moveSplitterLeft,QKeySequence(Qt.CTRL + Qt.Key_PageUp))
        interf_menu.addAction(self.style().standardIcon(QStyle.SP_ArrowRight),"Move splitter right",self.moveSplitterRight,QKeySequence(Qt.CTRL + Qt.Key_PageDown))

        # about window
        self.aboutWindow = uic.loadUi("%s/uis/about.ui"%os.path.dirname(os.path.realpath(sys.argv[0])))
        self.aboutWindow.parent = self
        self.aboutWindow.setWindowTitle('About GpxVid')
        ui = self.aboutWindow
        ui.logoLabel.setPixmap(QPixmap("%s/js/images/marker-icon.png"%os.path.dirname(os.path.realpath(sys.argv[0]))))
        txt = str(self.aboutWindow.infoLabel.text())
        txt = txt.replace('vvv',VERSION).replace('ddd',VERSION_DATE)
        self.aboutWindow.infoLabel.setText(txt)
        ui.okButton.clicked.connect(self.aboutWindow.close)

        help_menu = self.ui.menubar.addMenu("&Help")
        help_menu.addAction("&About",self.aboutWindow.show)

        # buttons

        self.ui.browseGpxButton.clicked.connect(self.loadGpx)
        self.ui.browseGpxButton.setIcon(self.style().standardIcon(QStyle.SP_DirOpenIcon))
        self.ui.browseVideoButton.clicked.connect(self.loadVideo)
        self.ui.browseVideoButton.setIcon(self.style().standardIcon(QStyle.SP_DirOpenIcon))

        self.ui.plustenButton.clicked.connect(self.plus10sec)
        self.ui.minustenButton.clicked.connect(self.minus10sec)
        self.ui.minustenButton.setIcon(self.style().standardIcon(QStyle.SP_MediaSeekBackward))
        self.ui.plustenButton.setIcon(self.style().standardIcon(QStyle.SP_MediaSeekForward))
        #self.ui.plustenButton.setShortcut(QKeySequence(Qt.ALT + Qt.Key_Right))
        #self.ui.minustenButton.setShortcut(QKeySequence(Qt.ALT + Qt.Key_Left))

        self.ui.gpxSlider.sliderMoved.connect(self.gpxSliderMoved)

        # layer combo

        for p in self.providerToUrl:
            self.ui.layerCombo.addItem(p)
        self.ui.layerCombo.currentIndexChanged.connect(self.mapProviderChanged)

        # map

        view = self.view = QtWebKitWidgets.QWebView(self)
        sp = QSizePolicy()
        sp.setVerticalPolicy(QSizePolicy.Expanding)
        sp.setHorizontalPolicy(QSizePolicy.Expanding)
        view.setSizePolicy(sp)

        cache = QtNetwork.QNetworkDiskCache()
        cache.setCacheDirectory(os.path.expanduser('~/.gpxvid/cache'))
        view.page().networkAccessManager().setCache(cache)
        view.page().networkAccessManager()

        view.page().mainFrame().addToJavaScriptWindowObject("MainWindow", self)
        view.page().setLinkDelegationPolicy(QtWebKitWidgets.QWebPage.DelegateAllLinks)
        #view.load(QtCore.QUrl('file:///home/julien/vcs/git/gpxvid/pyqt5leaflet/map.html'))
        view.load(QtCore.QUrl('file://%s/js/map.html'%os.path.dirname(os.path.realpath(sys.argv[0]))))
        view.loadFinished.connect(self.onLoadFinished)
        view.linkClicked.connect(QtGui.QDesktopServices.openUrl)

        self.ui.verticalLayout.insertWidget(2, view)

        # video player
        player = self.player = QMediaPlayer(self)
        self.player.durationChanged.connect(self.durationChanged)
        self.player.error.connect(self.mediaPlayerError)

        item = self.item = MyQVideoWidget(self)
        player.setVideoOutput(item)
        #gs = self.gs = QGraphicsScene(self.ui.graphicsView)
        #self.ui.graphicsView.setScene(gs)
        #self.ui.graphicsView.scene().addItem(item)
        self.ui.videoLayout.insertWidget(1, item)

        self.ui.splitter.setCollapsible(0,0)
        self.ui.splitter.setCollapsible(1,0)

        # control media player
        self.ui.playpauseButton.setIcon(self.style().standardIcon(QStyle.SP_MediaPlay))
        self.ui.playpauseButton.clicked.connect(self.playPauseControl)
        self.ui.playpauseButton.setShortcut(QKeySequence(Qt.Key_Space))
        self.player.positionChanged.connect(self.positionChanged)
        self.player.stateChanged.connect(self.stateChanged)
        self.ui.slider.sliderMoved.connect(self.seek)
        self.ui.volumeSlider.sliderMoved.connect(self.volumeChanged)
        self.ui.muteButton.setIcon(self.style().standardIcon(QStyle.SP_MediaVolume))
        self.ui.muteButton.setText('')
        self.ui.muteButton.clicked.connect(self.mutePressed)

        # final actions
        self.ui.latlngEdit.setFocus()

    def moveSplitterLeft(self):
        sizes = self.ui.splitter.sizes()
        sizes[0] -= 50
        sizes[1] += 50
        self.ui.splitter.setSizes(sizes)

    def moveSplitterRight(self):
        sizes = self.ui.splitter.sizes()
        sizes[0] += 50
        sizes[1] -= 50
        self.ui.splitter.setSizes(sizes)

    def zoomOut(self):
        frame = self.view.page().mainFrame()
        frame.evaluateJavaScript('zoomOut();')

    def zoomIn(self):
        frame = self.view.page().mainFrame()
        frame.evaluateJavaScript('zoomIn();')

    def toggleFollow(self):
        self.ui.followCheck.setChecked(not self.ui.followCheck.isChecked())

    def mutePressed(self):
        vol = self.ui.volumeSlider.value()
        if vol > 0:
            self.oldVolume = vol
            self.ui.volumeSlider.setValue(0)
            self.ui.muteButton.setIcon(self.style().standardIcon(QStyle.SP_MediaVolumeMuted))
        else:
            self.ui.volumeSlider.setValue(self.oldVolume)
            self.ui.muteButton.setIcon(self.style().standardIcon(QStyle.SP_MediaVolume))

        self.volumeChanged()

    def videoFullScreen(self):
        self.item.setFullScreen(True)

    def minus5volume(self):
        vol = self.ui.volumeSlider.value()
        self.ui.volumeSlider.setValue(vol-5)
        self.volumeChanged()

    def plus5volume(self):
        vol = self.ui.volumeSlider.value()
        self.ui.volumeSlider.setValue(vol+5)
        self.volumeChanged()

    def volumeChanged(self):
        vol = self.ui.volumeSlider.value()
        self.player.setVolume(vol)

    def mapProviderChanged(self):
        sel = str(self.ui.layerCombo.currentText())
        js = "changeTileLayer('%s', '%s');"%(self.providerToUrl[sel], self.providerToAttrib[sel])
        frame = self.view.page().mainFrame()
        frame.evaluateJavaScript("%s"%js)

    def loadGpx(self, checked=False, filePath=''):
        self.gpxCurrentSecond = 0

        if filePath == '':
            qfd = QFileDialog()
            filePath = str(qfd.getOpenFileName(parent=self, caption="Choose a GPX file", filter='Gpx file (*.gpx);; All files (*)')[0])

        if os.path.exists(filePath):
            # put the gpx track line on the map
            with open('%s'%filePath, 'r') as f:
                gpx_content = f.read()
                frame = self.view.page().mainFrame()
                frame.evaluateJavaScript("loadGpx('%s');"%gpx_content)
                self.ui.gpxPathEdit.setText(filePath)

            # load the gpx data
            self.secondToCoords = {}
            try:
                gpx = gpxpy.parse(gpx_content)
            except Exception as e:
                errorTxt = '%s'%e
                QMessageBox.critical(self, 'Gpx parsing error', 'Error in Gpx parsing :\n\n%s'%errorTxt)
                self.ui.gpxPathEdit.setText('')
                frame = self.view.page().mainFrame()
                frame.evaluateJavaScript('resetMap();')
                return

            self.firstTime = None
            lastPointPicked = None
            currentSecond = 0
            for track in gpx.tracks:
                for segment in track.segments:
                    for point in segment.points:
                        if point.time:
                            nbsec = point.time.second + (60 * point.time.minute) + (3600 * point.time.hour)
                        if self.firstTime == None and point.time:
                            self.firstTime = nbsec
                            self.secondToCoords[currentSecond] = [point.latitude, point.longitude, point.time, point.elevation, 0.0]
                            lastPointPicked = point
                        if point.time and self.firstTime != None:
                            # we take points with a minimum 1 sec delta
                            if (nbsec - self.firstTime) - currentSecond > 0:
                                currentSecond = (nbsec - self.firstTime)
                                # calculate speed
                                dist = distance(point, lastPointPicked)
                                nbsecBetweenPoints = nbsec - (lastPointPicked.time.second + (60 * lastPointPicked.time.minute) + (3600 * lastPointPicked.time.hour))
                                speed = (float(dist) / float(nbsecBetweenPoints)) * 3600 / 1000
                                # remember this point
                                self.secondToCoords[currentSecond] = [point.latitude, point.longitude, point.time, point.elevation, speed]
                                lastPointPicked = point

            for route in gpx.routes:
                for point in route.points:
                    if point.time:
                        nbsec = point.time.second + (60 * point.time.minute) + (3600 * point.time.hour)
                    if self.firstTime == None and point.time:
                        self.firstTime = nbsec
                        self.secondToCoords[currentSecond] = [point.latitude, point.longitude, point.time, point.elevation, 0.0]
                        lastPointPicked = point
                    if point.time and self.firstTime != None:
                        if (nbsec - self.firstTime) - currentSecond > 0:
                            currentSecond = (nbsec - self.firstTime)
                            # calculate speed
                            dist = distance(point, lastPointPicked)
                            nbsecBetweenPoints = nbsec - (lastPointPicked.time.second + (60 * lastPointPicked.time.minute) + (3600 * lastPointPicked.time.hour))
                            speed = (float(dist) / float(nbsecBetweenPoints)) * 3600 / 1000
                            # remember this point
                            self.secondToCoords[currentSecond] = [point.latitude, point.longitude, point.time, point.elevation, speed]
                            lastPointPicked = point

            self.ui.gpxSlider.setMaximum(currentSecond + 1)

    def loadVideo(self, checked=False, filePath=''):
        if filePath == '':
            qfd = QFileDialog()
            filePath = str(qfd.getOpenFileName(parent=self, caption="Choose a video file", filter='Video file (*.mp4 *.mov *.mkv *.avi *.ogv);; All files (*)')[0])

        if os.path.exists(filePath):
            self.player.setMedia(QMediaContent(QUrl('file://%s'%filePath)))
            # we play if a GPX is already loaded
            if os.path.exists(self.ui.gpxPathEdit.text()):
                self.player.play()
            self.ui.videoPathEdit.setText(filePath)
            self.lastVideoPosition = 0

    def minus5sec(self):
        self.player.setPosition(self.player.position() - 5000)

    def plus5sec(self):
        self.player.setPosition(self.player.position() + 5000)

    def minus10sec(self):
        self.player.setPosition(self.player.position() - 10000)

    def plus10sec(self):
        self.player.setPosition(self.player.position() + 10000)

    def minus1min(self):
        self.player.setPosition(self.player.position() - 60000)

    def plus1min(self):
        self.player.setPosition(self.player.position() + 60000)

    def seek(self, pos):
        self.player.setPosition(pos * 1000)

    def mediaPlayerError(self, error):
        if error == QMediaPlayer.FormatError:
            errorTxt = 'Video format not supported'
            QMessageBox.critical(self, 'Video player error', 'Error in video player :\n\n%s'%errorTxt)
            self.ui.videoPathEdit.setText('')

    def durationChanged(self, media):
        duration = self.player.duration()/1000
        self.ui.slider.setMinimum(0)
        self.ui.slider.setMaximum(duration)

    def positionChanged(self, pos):
        # text progress
        duration = self.player.duration()/1000
        position = int(pos/1000)
        self.ui.progressLabel.setText('%s/%s'%(self.timeFmt(position), self.timeFmt(duration)))
        # slider
        if not self.slider.isSliderDown():
            self.slider.setValue(position)

        videoAdvance = position - self.lastVideoPosition

        if self.player.state() == QMediaPlayer.PlayingState:
            # put marker
            self.gpxCurrentSecond += videoAdvance
            self.updateGpxMarker()

        self.lastVideoPosition = position

    def stateChanged(self, state):
        ''' sometimes, after asking for pause, the player jumps to the next second
        probably because it's to close. It caused a desync with the gpx.
        To solve this, we check if the video advanced on player state change.
        '''
        if state == QMediaPlayer.PausedState:
            position = int(self.player.position()/1000)
            videoAdvance = position - self.lastVideoPosition
            self.gpxCurrentSecond += videoAdvance
            self.updateGpxMarker()
            self.lastVideoPosition = position

            self.ui.gpxSliderLayout.removeItem(self.ui.spacer)
        else:
            self.ui.gpxSliderLayout.insertSpacerItem(0, self.ui.spacer)

        self.gpxSlider.setVisible(state == QMediaPlayer.PausedState)

    def updateGpxSlider(self):
        self.ui.gpxSlider.setValue(self.gpxCurrentSecond)
        maxi = self.ui.gpxSlider.maximum()
        self.ui.gpxSliderLabel.setText('%s/%s'%(self.timeFmt(self.gpxCurrentSecond), self.timeFmt(maxi)))
        self.lastGpxPosition = self.gpxCurrentSecond

    def updateHub(self, speed, ele):
        self.ui.elevationLabel.setText('%.2f'%ele)
        self.ui.speedLabel.setText('%.2f'%speed)

    def updateGpxMarker(self):
        #thepos = int(position)
        thepos = self.gpxCurrentSecond
        while thepos not in self.secondToCoords and thepos > 0:
            thepos -= 1
        point = self.secondToCoords[thepos]
        lat = point[0]
        lng = point[1]
        ele = point[3]
        speed = point[4]
        self.putMarkerOn(lat, lng, self.ui.followCheck.isChecked())
        self.updateGpxSlider()
        self.updateHub(speed, ele)

    def gpxSliderMoved(self, val):
        if self.player.state() == QMediaPlayer.StoppedState or self.player.state() == QMediaPlayer.PausedState:
            gpxAdvance = val - self.lastGpxPosition
            self.gpxCurrentSecond = val
            self.updateGpxMarker()

    def timeFmt(self, sec):
        if sec >= 3600:
            return '%s:%.2d:%.2d'%(sec/3600, (sec%3600)/60, sec%60)
        else:
            return '%.2d:%.2d'%(sec/60, sec%60)

    def playPauseControl(self):
        if self.player.state() == QMediaPlayer.StoppedState or self.player.state() == QMediaPlayer.PausedState:
            if os.path.exists(self.ui.videoPathEdit.text()):
                if os.path.exists(self.ui.gpxPathEdit.text()):
                    self.player.play()
                else:
                    QMessageBox.critical(self, 'Unable to play video', 'You must load a GPX file before playing a video')
        else:
            self.player.pause()

    def putMarkerOn(self, lat, lng, follow):
        frame = self.view.page().mainFrame()
        frame.evaluateJavaScript('putMarkerOn(%s, %s, %s);'%(lat, lng, str(follow).lower()))

    def resizeEvent(self, event):
        pass

    def onLoadFinished(self):
        with open('%s/js/map.js'%os.path.dirname(os.path.realpath(sys.argv[0])), 'r') as f:
            frame = self.view.page().mainFrame()
            frame.evaluateJavaScript(f.read())
        # play files given by CLI
        if self.gpxpath and self.videopath:
            self.loadGpx(filePath=self.gpxpath)
            self.loadVideo(filePath=self.videopath)

    @QtCore.pyqtSlot(float, float)
    def onMapMove(self, lat, lng):
        self.ui.latlngEdit.setText('Lng: {:.5f}, Lat: {:.5f}'.format(lng, lat))

    def panMap(self, lng, lat):
        frame = self.view.page().mainFrame()
        frame.evaluateJavaScript('map.panTo(L.latLng({}, {}));'.format(lat, lng))

if __name__ == '__main__':
    app = QApplication(sys.argv)

    # create .gpxvid
    if not os.path.exists(os.path.expanduser('~/.gpxvid')):
        os.mkdir(os.path.expanduser('~/.gpxvid'))

    # style
    if not sys.platform.startswith('darwin'):
        if "Breeze" in QStyleFactory.keys():
            app.setStyle("Breeze")
        else:
            app.setStyle("Fusion")

    # CLI args
    if len(sys.argv) > 2 and os.path.exists(sys.argv[1]) and os.path.exists(sys.argv[2]):
        myapp = Gpxvid(app, os.path.realpath(sys.argv[1]), os.path.realpath(sys.argv[2]))
    else:
        myapp = Gpxvid(app)
    myapp.show()
    sys.exit(app.exec_())
