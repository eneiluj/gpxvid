var map = L.map('map').setView([0, 0], 2);
L.control.scale({metric: true, imperial: true, position:'topleft'}).addTo(map);

var tilelayer = L.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    maxZoom: 18,
    attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
})

tilelayer.addTo(map);

function changeTileLayer(url, attrib){
    if (tilelayer !== null){
        map.removeLayer(tilelayer);
    }

    tilelayer = L.tileLayer(url, {
            maxZoom: 18,
            attribution: attrib
    });
    tilelayer.addTo(map);

}

var gpxlayer = null;

//var marker = L.marker(map.getCenter()).addTo(map);
//marker.bindPopup("Hello World!").openPopup();

function loadGpx(gpx){
    if (gpxlayer !== null){
        map.removeLayer(gpxlayer);
    }

    gpxlayer = new L.GPX(gpx, {
        async: false,
        marker_options: {
        startIconUrl: 'pin-icon-start.png',
        endIconUrl: 'pin-icon-end.png',
        shadowUrl: 'pin-shadow.png'
      }
    }).on('loaded', function(e) {
          map.fitBounds(e.target.getBounds());
    })
    gpxlayer.addTo(map);
}

if(typeof MainWindow != 'undefined') {
    var onMapMove = function() { MainWindow.onMapMove(map.getCenter().lat, map.getCenter().lng) };
    map.on('move', onMapMove);
    onMapMove();
}

var marker = null;

function putMarkerOn(lat, lng, follow){
    if (marker !== null){
        map.removeLayer(marker);
    }
    marker = L.marker([lat, lng]);
    marker.addTo(map);

    if (follow){
        map.panTo(L.latLng(lat, lng));
    }

}

function zoomIn(){
    map.zoomIn();
}

function zoomOut(){
    map.zoomOut();
}

function resetMap(){
    if (marker !== null){
        map.removeLayer(marker);
    }
    if (gpxlayer !== null){
        map.removeLayer(gpxlayer);
    }
    map.setView([0, 0], 2);
}
